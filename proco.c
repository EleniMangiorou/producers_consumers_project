#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "proco.h"


int main(int argc, char *argv[]) {


	circular_buffer *cb;
	cb = (circular_buffer*)malloc(sizeof (struct circular_buffer));

	if (argc != 6) {
		printf("ERROR: the program should take 5 arguments!\n");
		exit(-1);
	}

	int numberOfProducers = atoi(argv[1]);
	int numberOfConsumers = atoi(argv[2]);
	int randomNumbersPerProducer = atoi(argv[3]);
	size_t queueSize = (size_t) atoi(argv[4]);
	unsigned int seed = (unsigned) atoi(argv[5]);


	printf("Number of producers: %d \n", numberOfProducers);
	printf("Number of consumers: %d \n", numberOfConsumers);
	printf("randomNumbersPerProducer: %d \n", randomNumbersPerProducer);
	printf("Queue size: %d \n", queueSize);
	printf("seed: %d \n", seed);

	ProducerInputFile = fopen("producers_in.txt", "w");
	ConsumerInputFile = fopen("consumers_out.txt", "w");

	cb_init(cb, queueSize, sizeof(int));
	proco(cb, numberOfProducers, numberOfConsumers, randomNumbersPerProducer, queueSize, seed);


	printf("proco() returned... printing out results\n");

	int k, m;
	for (k = 0; k < numberOfProducers; k++) {
		printf("Producer %d : ", k+1);
		for (m = 0; m < randomNumbersPerProducer; m++) {
			if (m != 0) {
				printf(", ");
			};
			printf("%d", producerNumbersArray[k][m]);
		}
		printf("\n");
	}


	for (k = 0; k < numberOfConsumers; k++) {
		printf("Consumer %d : ", k+1);
		for (m = 0; m < consumerMaxNum[k]; m++) {
			if (m != 0) {
				printf(", ");
			};
			printf("%d", consumerNumbersArray[k][m]);
		}
		printf("\n");
	}


	free(tmp_read);// memory release 
	cb_free(cb);	// memory release 
	fclose(ProducerInputFile);
	fclose(ConsumerInputFile);
	return 1;

}
