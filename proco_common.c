#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include "proco.h"
#include <unistd.h>
#include <stdbool.h>


//initialize circular buffer
//capacity: maximum number of elements in the buffer
//sz: size of each element
void cb_init(circular_buffer *cb, size_t capacity, size_t sz)
{
    cb->buffer = malloc(capacity * sz);
    if (cb->buffer == NULL) {
        printf("Could not allocate memory..Exiting! \n");
        exit(1);
    }
    // handle error
    cb->buffer_end = (char *)cb->buffer + capacity * sz;
    cb->capacity = capacity;
    cb->count = 0;
    cb->sz = sz;
    cb->head = cb->buffer;
    cb->tail = cb->buffer;
}

//destroy circular buffer
void cb_free(circular_buffer *cb)
{
    free(cb->buffer);
}

//add item to circular buffer
void cb_push_back(circular_buffer *cb, const void *item)
{
    if (cb->count == cb->capacity)
    {
        printf("Access violation. Buffer is full\n");
        exit(1);
    }
    memcpy(cb->head, item, cb->sz);
    cb->head = (char*)cb->head + cb->sz;
    if (cb->head == cb->buffer_end)
        cb->head = cb->buffer;
    cb->count++;
}

//remove first item from circular buffer
void cb_pop_front(circular_buffer *cb, void *item)
{
    if (cb->count == 0)
    {
        printf("Access violation. Buffer is empy\n");
        exit(1);
    }
    memcpy(item, cb->tail, cb->sz);
    cb->tail = (char*)cb->tail + cb->sz;
    if (cb->tail == cb->buffer_end)
        cb->tail = cb->buffer;
    cb->count--;
}

bool cb_isEmpty(circular_buffer *cb) {
    if (cb->count == 0) {
        return true;
    }
    else
        return false;
}


//generate random numbers and put them in the cb queque
void *ProcuderJob(void *threadId) {

    PROCO_ARGUMENTS ProcoArgs;
    ProcoArgs = procoArgs;
    int *tid = (int *)threadId;
    int rc;
    int value;

    int i = 0;
    
    while ( (ProcoArgs.randomNumbersPerProducer) != i) {

        rc = pthread_mutex_lock(&mutexQ);

        //If the queue is full, wait until a consumer extracts a number
        //and calls you to start ((use of 'while' loop to avoid Spurious Wakeup)
        while (((ProcoArgs.cb)->count) == ((ProcoArgs.cb)->capacity)) {
            rc = pthread_cond_wait(&QueueEmpty, &mutexQ);
        }

        value = rand_r(&seed);

        cb_push_back(ProcoArgs.cb, (const void*) &value);
        fprintf(ProducerInputFile, "Producer %d: %d\n", *tid+1, value);
        fflush(ProducerInputFile);
        printf("Producer Thread %d: put the random number %d in queque.\n", *tid+1, value);

        producerNumbersArray[*tid][i] = value;

        //Notifies to the other threads, that it has started.
        rc = pthread_cond_signal(&QueueFull);
        rc = pthread_mutex_unlock(&mutexQ);
        i++;
    }
    printf("ProducerJob(): Consumer thread %d, is about to exit.\n", *tid);
    pthread_exit(threadId);
}

//extract the random numbers, one by one, from the cb queque
void *ConsumerJob(void *threadId) {

    PROCO_ARGUMENTS ProcoArgs;
    ProcoArgs = procoArgs;
    int *tid = (int *)threadId;
    int rc;
    int *tmp_read;
    tmp_read = (int*) malloc(sizeof(int)); // Allocates memory for char number

    int i = 0;

    while ( number_counter > 0 ) { 

        rc = pthread_mutex_lock(&mutexQ);
        //If the queue is empty, wait until a producer inserts a number
        //and calls you to start ((use of 'while' loop to avoid Spurious Wakeup)
        while ( ((ProcoArgs.cb)->count == 0) && (number_counter > 0) ) {
            rc = pthread_cond_wait(&QueueFull, &mutexQ);
            printf("ConsumerJob(): Consumer thread %d, the thread that will get a value from the queue has started.\n", *tid);
        }

        while ((ProcoArgs.cb)->count > 0) {
            cb_pop_front(ProcoArgs.cb, (void*)tmp_read);  //Reading from the buffer
            printf("Consumer %d: read number %d\n", *tid+1, *tmp_read);
            fprintf(ConsumerInputFile, "Consumer %d: %d\n", *tid+1, *tmp_read);
            fflush(ConsumerInputFile);
            number_counter--;
            printf("number_counter: %d\n", number_counter);
            consumerNumbersArray[*tid][i] = *tmp_read;
            consumerMaxNum[*tid] = i+1;
            i++;
        }

        //Notifies to the other threads, that it has started.
        rc = pthread_cond_signal(&QueueEmpty);
        rc = pthread_mutex_unlock(&mutexQ);        
    }
    printf("ConsumerJob(): Consumer thread %d, is about to exit.\n", *tid);
    pthread_exit(threadId);
}


void proco(circular_buffer * cb, int numberOfProducers, int numberOfConsumers, int randomNumbersPerProducer, size_t queueSize, unsigned int seedproco) {

    number_counter = numberOfProducers * randomNumbersPerProducer;

    int rc = 0 ;
    //Threads for producers
    pthread_t threadsProducer[numberOfProducers];
    int threadIdsProducer[numberOfProducers];

    //Threads for consumers
    pthread_t threadsConsumer[numberOfConsumers];
    int threadIdsConsumer[numberOfConsumers];

    procoArgs.cb = cb;
    procoArgs.numberOfProducers = numberOfProducers;
    procoArgs.numberOfConsumers = numberOfConsumers;
    procoArgs.randomNumbersPerProducer = randomNumbersPerProducer;
    procoArgs.queueSize = queueSize;
    procoArgs.seed = seedproco;
    seed = procoArgs.seed * time(NULL);


    //Initialization of the producer mutex
    rc = pthread_mutex_init(&mutexQ, NULL);
    /*Initialization of the Producer's conditional variable. */
    rc = pthread_cond_init(&QueueFull, NULL);
    /*Initialization of the Consumer's conditional variable. */
    rc = pthread_cond_init(&QueueEmpty, NULL);

    int i;
    printf("Creating producer numbers array. Size %dx%d\n", numberOfProducers, randomNumbersPerProducer);
    producerNumbersArray = (int **) malloc( numberOfProducers * sizeof(int *) );
    for (i = 0; i < numberOfProducers; i++) {
        producerNumbersArray[i] = (int *) malloc( randomNumbersPerProducer * sizeof(int));
    }

    int j;
    consumerNumbersArray = (int **) malloc( (numberOfConsumers) * sizeof(int *) );
    for (j = 0; j < numberOfConsumers; j++) {
        consumerNumbersArray[j] = (int *) malloc( (numberOfProducers * randomNumbersPerProducer) * sizeof(int) );
    }
    consumerMaxNum = (int *) malloc( (numberOfConsumers) * sizeof(int));


    // Creating threads
    for (i = 0; i < numberOfProducers; i++) {
        threadIdsProducer[i] = i;
        printf("Main: creating Producer thread %d\n", threadIdsProducer[i]);
        rc = pthread_create(&threadsProducer[i], NULL, ProcuderJob, &threadIdsProducer[i]);
    }

    for (j = 0; j < numberOfConsumers; j++) {
        threadIdsConsumer[j] = j;
        printf("Main: creating Consumer thread %d\n", threadIdsConsumer[j]);
        rc = pthread_create(&threadsConsumer[j], NULL, ConsumerJob, &threadIdsConsumer[j]);
    }

    //Join of Producer threads
    void *statusProducer;
    for (i = 0; i < numberOfProducers; i++) {
        rc = pthread_join(threadsProducer[i], &statusProducer);
        printf("Main: Producer Thread %d finished with status %d.\n", threadIdsProducer[i], *(int *)statusProducer);
    }


    printf("Waiting for consumer threads to join...\n");

    rc = pthread_cond_broadcast(&QueueFull);

    //Join of Consumers threads
    void *statusConsumer;
    for (j = 0; j < numberOfConsumers; j++) {
        rc = pthread_join(threadsConsumer[j], &statusConsumer);

        if (rc != 0) {
            printf("ERROR: return code from pthread_join() is %d\n", rc);
            exit(-1);
        }

        printf("Main: Consumer Thread %d finished with status %d.\n", threadIdsConsumer[j], *(int *)statusConsumer);
    }

    /* Destruction of the mutex. */
    rc = pthread_mutex_destroy(&mutexQ);
    if (rc != 0) {
        printf("ERROR: return code from pthread_mutex_destroy() of mutex is %d\n", rc);
        exit(-1);
    }

    /*Destruction of the Producer's condition variable. */
    rc = pthread_cond_destroy(&QueueFull);
    if (rc != 0) {
        printf("ERROR: return code from pthread_cond_destroy() of Producer condition %d\n", rc);
        exit(-1);
    }

    /*Destruction of the Consumer's condition variable. */
    rc = pthread_cond_destroy(&QueueEmpty);
    if (rc != 0) {
        printf("ERROR: return code from pthread_cond_destroy() of Consumer condition is %d\n", rc);
        exit(-1);
    }

}
