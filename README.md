#Description

This a semester project for the Operating Systems laboratory at the Informatics Deparment of the Athens University of Economics and Business. The goal was to implement a producer-consumer system using the POSIX threads package (pthreads).
In this program, one or more producers threads generate random numbers and enter them at one end of a circular buffer queue, while one or more consumer threads export numbers from the other end of the queue. There must not be simultaneous import or export from the queue (mutual exclusion). In addition, producers should be blocked when the queue is full, while consumers must be prevented to access the queue, when it is empty (synchronization).

Input
-

This program receives five parameters in the specific order: the number of producers, the number of consumers, the total of random numbers that each producer will produce, the size of the queue and a positive integer as a seed for the pseudorandom number generator.
For example, the command `proco 4 2 20 10 7` creates four producers and two consumers, each of the producers will produce 20 random numbers and will try to insert them in a queue with a size of 10, while the seed of the pseudorandom number generator will be 7.

Output
-

Every producer, when it inserts a number in the queue, it also inserts a record line with it's identity and the specific number into the file ./producers_in.txt.
Every consumer, when it extracts a number from the queue, it inserts a record line with it's identity and the specific number into the file ./consumers_out.txt.
The order of rows is random, but the lines are not tangled with one another. 
At the end of execution of the program, the numbers which each producer has inserted in the queue, are initially shown on the screen, and subsequently, each number that a consumer has extracted from the queue is also printed on the screen.


Installation
-

In order to clone the repository open a command line and issue

`git clone https://bitbucket.org/EleniMangiorou/producers_consumers_project.git`

Enter the repository directory

`cd producers_consumers_project`

To build the program, issue

`gcc -pthread -o proco proco.c proco_common.c`

And run the program with the command

`./proco 2 3 7 1 3`

