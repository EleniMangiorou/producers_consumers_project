#include <pthread.h>
#include "stdio.h"
#include "sys/types.h"
#include "stdlib.h"
#include "string.h"
#include <unistd.h>
#include <stdbool.h>

int number_counter;
unsigned int seed;

typedef struct circular_buffer
{
	void *buffer;     // data buffer
	void *buffer_end; // end of data buffer
	size_t capacity;  // maximum number of items in the buffer
	size_t count;     // number of items in the buffer
	size_t sz;        // size of each item in the buffer
	void *head;       // pointer to head
	void *tail;       // pointer to tail
} circular_buffer;

typedef struct proco_arguments {
	circular_buffer *cb;
	int numberOfProducers;
	int numberOfConsumers;
	int randomNumbersPerProducer;
	size_t queueSize;
	unsigned int seed;
} PROCO_ARGUMENTS;


/*
 * The mutex, which is used for the protection of the common buffer, 
 * is external variable as to be visible from the threads.
*/
//mutex for the Queue
pthread_mutex_t mutexQ;


//mutex for producers
pthread_cond_t QueueFull;
//mutex for consumers
pthread_cond_t QueueEmpty;

PROCO_ARGUMENTS procoArgs;
FILE *ProducerInputFile;
FILE *ConsumerInputFile;
int *tmp_read ;

int ** producerNumbersArray;
int ** consumerNumbersArray;
int * consumerMaxNum;

void proco(circular_buffer *cb, int producer, int consumer, int numbersRandom, size_t sizequeue, unsigned int seed) ;

void *ConsumerJob(void *threadId);

void *ProcuderJob(void *threadId);


//initialize circular buffer
//capacity: maximum number of elements in the buffer
//sz: size of each element
void cb_init(circular_buffer *cb, size_t capacity, size_t sz);

//destroy circular buffer
void cb_free(circular_buffer *cb);

//add item to circular buffer
void cb_push_back(circular_buffer *cb, const void *item);

//remove first item from circular item
void cb_pop_front(circular_buffer *cb, void *item);

//check if the circular buffer is empty
bool cb_isEmpty(circular_buffer *cb);


